// @ts-check
import { defineConfig } from "astro/config";

import react from "@astrojs/react";

import node from "@astrojs/node";

import sitemap from "@astrojs/sitemap";

// https://astro.build/config
export default defineConfig({
  site: "https://owni.dev",
  output: "server",
  integrations: [react(), sitemap()],

  i18n: {
    defaultLocale: "en",
    locales: ["fr", "en"],
  },

  adapter: node({
    mode: "standalone",
  }),
});