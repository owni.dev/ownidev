interface ProjectItem {
  id: string;
  client?: string;
  title: string;
  feature_image: string;
  description: string;
  time_spent: string;
  tags: string;
  url?: string;
}
interface BlogPost {
  id: number;
  title: string;
  tags: string;
  content: string;
  slug: string;
  published_at: Date;
  feature_image: ImageFormats;
}
type ImageFormats = {
  original: string;
  thumb: string;
};
