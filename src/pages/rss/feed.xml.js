import rss from "@astrojs/rss";
import sanitizeHtml from "sanitize-html";

export async function GET(context) {
  return rss({
    title: "OwNi.dev news and posts",
    description:
      "Save this feed in your reader for short code tutorials, pre-social networks internet nostalgia and possibly some comics.",
    site: context.site,
    stylesheet: "/pretty-feed-v3.xsl",
    items: await (async () => {
      const res = await fetch(import.meta.env.PUBLIC_API_URL + "/posts/");
      const data = (await res.json()).data;
      return data.map((post) => ({
        title: post.title,
        pubDate: post.published_at,
        description: sanitizeHtml(post.content.substring(0, 120)),
        link: `/post/${post.slug}`,
      }));
    })(),
  });
}
