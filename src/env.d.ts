/// <reference path="../.astro/types.d.ts" />
declare namespace App {
  interface Locals {
    BlogPost: import("./global.d.ts").BlogPost;
    ProjectItem: import("./global.d.ts").ProjectItem;
    ImageFormats: import("./global.d.ts").ImageFormats;
  }
}
