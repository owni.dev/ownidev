import axios, { AxiosError, type AxiosResponse } from "axios";

//https://stackoverflow.com/questions/47216452/how-to-handle-401-authentication-error-in-axios-and-react
const privateAPI = axios.create({
  baseURL: "/api/admin",
  headers: {
    "Content-Type": "application/json", // Set the default content type for requests
  },
});
privateAPI.interceptors.response.use(
  (response: AxiosResponse) => {
    return response;
  },
  (error: AxiosError) => {
    if (error.status == 401) {
      // Log out user here
      return (window.location.href = "/login");
    }
    return Promise.reject(error);
  }
);

const publicAPI = axios.create({
  baseURL: "/api",
  headers: {
    "Content-Type": "application/json", // Set the default content type for requests
  },
});

export { privateAPI, publicAPI };
