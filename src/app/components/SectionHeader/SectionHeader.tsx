import "./sectionHeader.sass";

interface IProps {
  title: string;
  tagline: string;
  as?: "h1" | "h2";
}

const SectionHeader = ({ title, as = "h2" }: IProps) => {
  return (
    <header className="project__title">
      {as == "h1" ? <h1>{title}</h1> : <h2>{title}</h2>}
    </header>
  );
};
export default SectionHeader;
