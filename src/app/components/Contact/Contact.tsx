import { useForm, type SubmitHandler } from "react-hook-form";

import { getLangFromUrl, useTranslations } from "../../../i18n/utils.ts";
import { publicAPI } from "~/lib/axios-instance";

import SectionHeader from "../SectionHeader/SectionHeader";
import "./contact.sass";
import { toast } from "react-hot-toast";
import { useState } from "react";

interface IFormInput {
  contact: string;
  identity: "business" | "professional" | "person" | "student" | "charity";
  message: string;
}

const Contact = () => {
  //@ts-ignore
  const lang = getLangFromUrl(window.location);
  const t = useTranslations(lang, { keyPrefix: "contact" });
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<IFormInput>();

  const [loading, setLoading] = useState(false);

  const options = [
    { name: t("identity.business"), value: "business" },
    { name: t("identity.pro"), value: "professional" },
    { name: t("identity.person"), value: "person" },
    { name: t("identity.student"), value: "student" },
    { name: t("identity.charity"), value: "charity" },
  ];

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    setLoading(true);
    const promise = publicAPI.post(
      import.meta.env.PUBLIC_API_URL + "/inquiries",
      {
        inquiry: data,
      }
    );
    toast.promise(promise, {
      loading: "Sending message",
      success: "Message sent !",
      error: "An error happened! Please try again later",
    });
    promise.then(() => setLoading(false));
  };
  return (
    <section id="contact-me">
      <SectionHeader title={t("title")} tagline={t("tagline")} />
      <form className="contact__content" onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group contact__content__identity">
          <label htmlFor="identity">{t("identity.who")}</label>
          <div className="select">
            <select
              id="identity"
              {...register("identity", { required: true })}
              aria-invalid={errors.identity ? "true" : "false"}
            >
              {options.map((option, index) => (
                <option value={option.value} key={index}>
                  {option.name}
                </option>
              ))}
            </select>
          </div>
        </div>

        <div className="form-group contact__content_email">
          <label htmlFor="contact">{t("email.label")}</label>

          <div className="input">
            <input
              autoComplete="off"
              autoCorrect="off"
              aria-invalid={errors.contact ? "true" : "false"}
              className={errors.contact ? "invalid" : ""}
              id="contact"
              type="email"
              {...register("contact", { required: t("email.required") })}
            />
            <span className="border"></span>
          </div>
          {errors.contact && (
            <p role="alert">
              {errors.contact.type === "required" && errors.contact.message}
            </p>
          )}
        </div>
        <div className="form-group contact__content_message">
          <label htmlFor="message">{t("message.label")}</label>
          <div className="input">
            <textarea
              autoComplete="off"
              autoCorrect="off"
              autoCapitalize="off"
              aria-invalid={errors.message ? "true" : "false"}
              className={errors.message ? "invalid" : ""}
              id="message"
              {...register("message", {
                required: t("message.required"),
                minLength: 100,
              })}
              rows={10}
            />

            <span className="border"></span>
          </div>
          {errors.message && (
            <p role="alert">
              {errors.message.type === "required" && errors.message.message}
            </p>
          )}
        </div>
        <button
          type="button"
          onClick={handleSubmit(onSubmit)}
          disabled={loading}
        >
          {t("submit")}
        </button>
      </form>
    </section>
  );
};
export default Contact;
