interface IProps {
  image: string;
  alt: string;
  text: string;
}

const StackItem = ({ image, alt, text }: IProps) => {
  return (
    <div className="stack__item">
      <img src={image} alt={alt} />
      <p>{text}</p>
    </div>
  );
};
export default StackItem;
