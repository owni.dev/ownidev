import "./tempHeader.sass";
const TempHeader = () => {
  return (
    <div className="temp-header">
      <p className="wip">Currently modeling this</p>
      <img
        src="./tempheader.webp"
        alt="A hand drawn sketch depicting a soup pack, a bowl and a spoon. Notes for each elements are scribbled around the page"
      />
    </div>
  );
};
export default TempHeader;
