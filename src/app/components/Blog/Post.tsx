import sanitizeHtml from "sanitize-html";
import "./post.sass";
import hljs from "highlight.js/lib/core";
import "highlight.js/styles/base16/zenburn.min.css";
import javascript from "highlight.js/lib/languages/javascript";
import elixir from "highlight.js/lib/languages/elixir";
import css from "highlight.js/lib/languages/css";
import { useEffect } from "react";

const Post = ({
  post,
}: {
  post: {
    title: string;
    slug: string;
    content: string;
    feature_image: { original: string; thumbnail: string };
  };
}) => {
  hljs.registerLanguage("javascript", javascript);
  hljs.registerLanguage("elixir", elixir);
  hljs.registerLanguage("css", css);
  useEffect(() => {
    hljs.highlightAll();
  }, []);
  if (post == undefined) {
    return (
      <article id="post">
        <p>The post could not be found !</p>
      </article>
    );
  }
  return (
    <article id="post">
      <img
        src={post.feature_image ? post.feature_image.original : ""}
        alt={post.title}
      />
      <header>
        <h1>{post.title ? post.title : "No title"}</h1>
        <sup>{post.slug ? post.slug : "No slug"}</sup>
      </header>
      <div
        dangerouslySetInnerHTML={{
          __html: post.content
            ? sanitizeHtml(post.content)
            : "<p>No content</p>",
        }}
        id="content"
      />
    </article>
  );
};
export default Post;
