import { Canvas } from "@react-three/fiber";
import { PerspectiveCamera } from "@react-three/drei";

import { Model } from "~/components/Model/Model";
import "./threeHero.sass";

const ThreeHero = () => {
  return (
    <div id="three-hero">
      <Canvas shadows>
        <ambientLight intensity={1} />
        <directionalLight
          intensity={10}
          position={[0.5, 5, -2]}
          color="#fefeee"
        />
        <directionalLight
          intensity={1}
          position={[0.5, 2, -1]}
          color="#FF5733"
        />
        <PerspectiveCamera
          makeDefault
          far={100}
          near={0.1}
          fov={50}
          position={[0, 3, 8]}
        >
          <directionalLight
            castShadow
            position={[10, 20, 15]}
            shadow-camera-right={8}
            shadow-camera-top={8}
            shadow-camera-left={-8}
            shadow-camera-bottom={-8}
            shadow-mapSize-width={1024}
            shadow-mapSize-height={1024}
            intensity={10}
            shadow-bias={-0.0001}
          />
        </PerspectiveCamera>
        <Model scale={4} rotation={[0, -1.13, 0]} position={[0, 0, 0]} />
      </Canvas>
    </div>
  );
};
export default ThreeHero;
