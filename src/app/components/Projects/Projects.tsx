import "./projects.sass";
import { motion, useAnimate } from "framer-motion";
import { useEffect, useState } from "react";
import { getLangFromUrl, useTranslations } from "../../../i18n/utils.ts";
import SectionHeader from "../SectionHeader/SectionHeader";
import axios, { AxiosError } from "axios";
import sanitizeHtml from "sanitize-html";

const Projects = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<Array<ProjectItem | undefined>>([]);
  const [scope, animate] = useAnimate();
  const [lastAnimated, setLastAnimated] = useState<null | HTMLDivElement>(null);
  //@ts-ignore
  const lang = getLangFromUrl(window.location);
  const t = useTranslations(lang, { keyPrefix: "projects" });

  useEffect(() => {
    setLoading(true);
    axios
      .get(import.meta.env.PUBLIC_API_URL + "/projects")
      .then((res) => {
        setData(res.data.data);
        setLoading(false);
      })
      .catch((_error: AxiosError) => {
        setLoading(false);
      });
  }, []);
  const activated = {
    scale: 1,
    boxShadow: "rgba(114, 120, 167, 0.2) 10px 12px 13px 0px",
    y: 0,
    zIndex: 10,
    width: "35vw",
    rotate: 0,
  };
  const initial = {
    zIndex: 9,
    scale: 0.9,
    boxShadow: "5px -1px 5px 0px rgba(114, 120, 167, 0.2)",
    y: Math.floor(Math.random() * 30),
    width: "28vw",
    rotate: Math.floor(Math.random() * (12 - -6) + -6),
  };
  function animateOnTap(event: MouseEvent | TouchEvent | PointerEvent) {
    const clickedItem = event.target as HTMLDivElement;
    if (
      //@ts-ignore
      clickedItem.attributes.type &&
      //@ts-ignore
      clickedItem.attributes.type.value == "button"
    )
      return;
    const card = clickedItem.closest(".project__card") as HTMLDivElement;
    animate(card, activated);

    if (lastAnimated == card) {
      reset(card);
      setLastAnimated(null);
    } else {
      reset(lastAnimated);
      setLastAnimated(card);
    }
  }
  function reset(element: HTMLDivElement | null) {
    if (element == null) return;
    animate(element, initial);
  }

  return (
    <section id="projects">
      <SectionHeader title={t("projects")} tagline={t("tagline")} />
      <div className="project__cards" ref={scope}>
        {loading && <p>Loading</p>}
        {!loading &&
          data !== undefined &&
          data.map((el, index) => {
            if (el == undefined) {
              return;
            }
            return (
              <div
                className="project__cards__card"
                id={`key-${index}`}
                key={index}
              >
                <motion.div
                  initial={{ ...initial, zIndex: 10 - index }}
                  whileHover={activated}
                  whileFocus={activated}
                  onTap={animateOnTap}
                  whileTap={{ scale: 0.8 }}
                  className="project__card"
                >
                  <h3 className="project__card__title">{el.title}</h3>
                  <div className="project__card__tags">
                    {el.tags !== "" &&
                      el.tags.split(",").map((tag: string, index: number) => (
                        <span key={index} className="project__card__tags tag">
                          {tag}
                        </span>
                      ))}
                  </div>
                  <img
                    className="project__card__image"
                    src={el.feature_image}
                  />
                  <p
                    className="project__card__description"
                    dangerouslySetInnerHTML={{
                      __html: sanitizeHtml(el.description),
                    }}
                  />

                  <a
                    href={el.url}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="project__card__description project__card__link"
                  >
                    <button type="button">Visit</button>
                  </a>
                </motion.div>
              </div>
            );
          })}
      </div>
    </section>
  );
};
export default Projects;
