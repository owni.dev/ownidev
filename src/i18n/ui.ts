export const languages = {
  en: "English",
  fr: "Français",
};

export const defaultLang = "en";
export const showDefaultLang = false;

export const ui = {
  en: {
    greetings: {
      wip: "Work in progress",
      tagline: "Posts and projects",
      soup: "Soup dev extraordinaire",
      good: "Good web is like good soup",
      simple: "The web made simple and easy",
      pro: "Fullstack Developer",
      nicer: "The nicer web",
      you: "What can I do for you today ?",
      stock: "Something something stock and stack",
    },
    projects: {
      projects: "Projects",
      tagline: "Things I cooked up",
    },
    contact: {
      title: "Contact",
      tagline: "Let's chat",
      identity: {
        who: "Who am I talking to ?",
        business: "A business",
        pro: "A professional",
        person: "A customer",
        student: "A student",
        charity: "A charity or an NPO",
      },
      message: {
        label: "Your message",
        required: "Your message must include something",
      },
      email: {
        label: "Your e-mail",
        required: "Please provide an e-mail so I can get back to you",
      },
      submit: "Contact me !",
    },
    about: {
      title: "About",
      tagline: "Story time !",
      intro:
        "I am a freelance developer, mostly front-end but with a passion to understand all things that makes me also able to work on back-end and CI/CD.",
      values:
        "As much as I would rather create flawless pieces of code, I do enjoy the bug squishing and problem solving part of the job.",
      "hard-skills":
        "To build your projects, my tech of choice is ReactJS (javascript) with a whole 5 years of experience, as well as a newly adopted Elixir I fell in love with recently. My skills also extend (not as good as the other two) to generally anything javascript, Ruby on Rails, Python, PHP and web design",
      "soft-skills":
        "I believe in transparency, a good work/life balance and owning up to your mistakes. I'm motivated by the need for a challenge and improvement and any over-the-top organisation (tell me all about your workflows).",
      others:
        "During my freetime, I like to model objects to 3D print, fine-tune home-automation, discover opensource projects, lift weights and draw comics",
    },
    stack: {
      title: "Tools",
      tagline: "The standmixer of web development",
      main: "Main stack",
      others: "Can also do",
    },
  },
  fr: {
    greetings: {
      wip: "En cours de développement",
      tagline: "Blog et projets",
      soup: "Soup dev extraordinaire",
      good: "Du bon code, c'est comme une bonne soupe",
      simple: "Le web mais en plus simple",
      pro: "Développeur Fullstack",
      nicer: "Le côté cool de l'Internet",
      you: "Que puis-je faire pour vous aujourd'hui ?",
      stock: "Ce jeu de mot ne se traduit pas en Français",
    },
    projects: {
      projects: "Projets",
      tagline: "Fait avec amour",
    },
    contact: {
      title: "Contact",
      tagline: "Parlez-moi un peu de vous.",
      identity: {
        who: "À qui ai-je l'honneur ?",
        business: "Une entreprise",
        pro: "Un.e professionnel.le",
        person: "Un particulier",
        student: "Un.e étudiant.e",
        charity: "Une association / organisme à but non lucratif",
      },
      message: {
        label: "Votre message",
        required: "Votre message doit inclure du texte !",
      },
      email: {
        label: "Votre e-mail",
        required:
          "Laissez-moi votre e-mail afin que je puisse vous recontacter",
      },
      submit: "Contactez moi !",
    },
    about: {
      title: "À propos",
      tagline: "C'est l'histoire de...",
      intro:
        "Développeur freelance, Owen se fait une joie de vous accompagner vous et votre équipe dans vos différents projets en développement web.",
      values:
        "En plus d'aimer les challenges et le débuggage, c'est apporter de l'aide aux autres, à vous comme à vos clients, qui motive Owen à pousser ses limites et toujours aller de l'avant.",
      "hard-skills":
        "Pour réaliser vos projets, ses outils de prédilection sont ReactJS (Javascript) avec 5 ans d'expérince ainsi qu'Elixir tout récemment. Ses compétences s'étendent aussi (mais en moins bien !) en Ruby on rails, Python, PHP et design web.",
      "soft-skills":
        "Ajoutez en plus de cela une touche de bonne humeur, de la rigueur, une organisation peut-être surfaite (parlez-moi de vos workflows), une envie d'évolution constante ainsi que des capacités en gestion de projets.",
      others:
        "À ses heures perdues, Owen s'égare à la modelisation d'objets pour son imprimante 3D, l'automatisation de sa maison, la découverte de projets opensource, le soulevage de fonte ainsi qu'à créer des bandes-dessinées.",
    },
    stack: {
      title: "Outils",
      tagline: "Tout le nécessaire",
      main: "Principalement :",
      others: "Entre autres :",
    },
  },
} as const;
