import { ui, defaultLang, showDefaultLang } from "./ui";

export function getLangFromUrl(url: URL) {
  const [, lang] = url.pathname.split("/");
  if (lang in ui) return lang as keyof typeof ui;
  return defaultLang;
}

export function useTranslatedPath(lang: keyof typeof ui) {
  return function translatePath(path: string, l: string = lang) {
    return !showDefaultLang && l === defaultLang ? path : `/${l}${path}`;
  };
}
export function useTranslations(
  lang: keyof typeof ui,
  options: { keyPrefix?: string } = {}
) {
  return function t(key: keyof (typeof ui)[typeof defaultLang] | any) {
    let baseKey = [];
    if (options.keyPrefix != null) {
      baseKey =
        //@ts-ignore
        ui[lang][options.keyPrefix] || ui[defaultLang][options.keyPrefix];
    } else {
      //@ts-ignore
      baseKey = ui[lang] || ui[defaultLang];
    }
    const value = key.split(".").reduce(function (prev: any, key: any) {
      return prev[key];
    }, baseKey);
    return value;
  };
}
